import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
	providedIn: 'root'
})
export class AuthService {
	url = 'endpoint';

	constructor(private http: HttpClient) { }

	login(body) {
		return this.http.post(this.url, body)
	}

	ajaxLogin(body, successFunction, failFunction) {
		const xhttp = new XMLHttpRequest()
		xhttp.onreadystatechange = function(res) {
			if (this.readyState == 4) {
				if (this.status == 200) {
					successFunction(res)
				} else {
					failFunction(res)
				}
			}
		}
		xhttp.open("POST", this.url, true)
		xhttp.send()
	}

}
