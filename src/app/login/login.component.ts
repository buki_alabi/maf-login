import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms'

import { AuthService } from '../services/auth/auth.service'

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	@ViewChild('loginForm') form: NgForm;
	body = {
		username: undefined,
		password: undefined
	}

	constructor(private auth: AuthService) { }

	ngOnInit() {
	}

	submit() {
		if (!this.form.valid) {
			for (let control of Object.values(this.form.controls)) {
				control.markAsDirty()
			}
			return;
		}
		this.ajaxLogin()
	}

	login() {
		this.auth.login(this.body).subscribe((res: any) => this.success(res), (err: any) => this.fail(err))
	}

	ajaxLogin() {
		this.auth.ajaxLogin(this.body, (res: any) => this.success(res), (err: any) => this.fail(err))
	}


	success(res: any) {
		console.log("res: ", res)
		this.form.reset()
	}

	fail(err: any) {
		console.error("Err: ", err)
		this.form.reset()
	}

}
