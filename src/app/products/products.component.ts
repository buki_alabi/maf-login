import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-products',
	templateUrl: './products.component.html',
	styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

	products = [
		{
			name: "iPhone 6 Black",
			price: "AED 1000",
		},
		{
			name: "iPhone 7 Black",
			price: "AED 2000",
		},
		{
			name: "iPhone 8 Black",
			price: "AED 3000",
		},
	]

	constructor() { }

	ngOnInit() {
	}

}
